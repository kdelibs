/*
 * Copyright (C) 2007 Apple Inc. All rights reserved.
 * Copyright (C) 2007 David Smith (catfish.man@gmail.com)
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1.  Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 * 2.  Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in the
 *     documentation and/or other materials provided with the distribution.
 * 3.  Neither the name of Apple Computer, Inc. ("Apple") nor the names of
 *     its contributors may be used to endorse or promote products derived
 *     from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY APPLE AND ITS CONTRIBUTORS "AS IS" AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL APPLE OR ITS CONTRIBUTORS BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 * THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "ClassNodeList.h"

#include "Document.h"
#include "misc/htmlattrs.h"

namespace DOM {

ClassNodeList::ClassNodeList(NodeImpl* rootNode, const DOMString& classNames)
    : NodeListImpl(rootNode, UNCACHEABLE)
{
    m_classNames.parseClassAttribute(classNames.string(), m_refNode->document()->inCompatMode());
}

bool ClassNodeList::nodeMatches(NodeImpl *testNode, bool& doRecurse) const
{
    if (!testNode->isElementNode()) {
        doRecurse = false;
        return false;
    }

    if (!testNode->hasClass())
        return false;

    if (!m_classNames.size())
        return false;

    ClassNames classes;
    ElementImpl* el = static_cast<ElementImpl*>(testNode);
    const DOMString cn = el->getAttribute(ATTR_CLASS);
    bool const compat = el->document()->inCompatMode();
    classes.parseClassAttribute(cn, compat);
    for (size_t i = 0; i < m_classNames.size(); ++i) {
        if (!classes.contains(m_classNames[i]))
            return false;
    }

    return true;
}

} // namespace DOM

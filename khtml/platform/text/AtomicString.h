#ifndef AtomicString_h
#define AtomicString_h

#include "dom/dom_string.h"

namespace WebCore
{
    using namespace DOM;
    typedef DOMString AtomicString;
}

#endif // AtomicString_h

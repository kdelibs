/**
  * This file is part of the KDE libraries
  * Copyright (C) 2007 Rafael Fernández López <ereslibre@kde.org>
  * Copyright (C) 2003 Anders Lund <anders.lund@lund.tdcadsl.dk>
  *
  * This library is free software; you can redistribute it and/or
  * modify it under the terms of the GNU Library General Public
  * License version 2 as published by the Free Software Foundation.
  *
  * This library is distributed in the hope that it will be useful,
  * but WITHOUT ANY WARRANTY; without even the implied warranty of
  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  * Library General Public License for more details.
  *
  * You should have received a copy of the GNU Library General Public License
  * along with this library; see the file COPYING.LIB.  If not, write to
  * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
  * Boston, MA 02110-1301, USA.
  */

#ifndef DOCWORDCOMPLETION_CONFIG_H
#define DOCWORDCOMPLETION_CONFIG_H

#include <kcmodule.h>

class DocWordCompletionConfig
    : public KCModule
{
    Q_OBJECT

public:
    explicit DocWordCompletionConfig(QWidget *parent, const QVariantList &);
    virtual ~DocWordCompletionConfig();

    virtual void save();
    virtual void load();
    virtual void defaults();

private Q_SLOTS:
    void slotChanged();

private:
    class KHBox *hbTreshold;
    class QCheckBox *cbAutoPopup;
    class QSpinBox *sbAutoPopup;
    class QLabel *lSbRight;
};

#endif // DOCWORDCOMPLETION_CONFIG_H
